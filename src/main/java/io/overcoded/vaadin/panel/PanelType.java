package io.overcoded.vaadin.panel;

import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum PanelType {
    PRIMARY(PanelConfig
            .builder()
            .primaryColor("var(--lumo-primary-color)")
            .backgroundColor("var(--lumo-primary-color-10pct)")
            .icon(VaadinIcon.INFO_CIRCLE)
            .build()),
    SECONDARY(PanelConfig
            .builder()
            .primaryColor("var(--lumo-contrast)")
            .backgroundColor("var(--lumo-contrast-10pct)")
            .variant(ButtonVariant.LUMO_CONTRAST)
            .icon(VaadinIcon.ASTERISK)
            .build()),
    SUCCESS(PanelConfig
            .builder()
            .primaryColor("var(--lumo-success-color)")
            .backgroundColor("var(--lumo-success-color-10pct)")
            .variant(ButtonVariant.LUMO_SUCCESS)
            .icon(VaadinIcon.CHECK_CIRCLE)
            .build()),
    WARNING(PanelConfig
            .builder()
            .primaryColor("hsl(32, 100%, 30%)")
            .backgroundColor("hsla(48, 100%, 50%, 0.1)")
            .variant(ButtonVariant.LUMO_ERROR)
            .icon(VaadinIcon.WARNING)
            .build()),
    ERROR(PanelConfig
            .builder()
            .primaryColor("var(--lumo-error-color)")
            .backgroundColor("var(--lumo-error-color-10pct)")
            .variant(ButtonVariant.LUMO_ERROR)
            .icon(VaadinIcon.EXCLAMATION_CIRCLE)
            .build());

    private final PanelConfig config;
}
