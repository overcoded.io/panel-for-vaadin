package io.overcoded.vaadin.panel;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.icon.IconFactory;

public class PanelFactory {
    public Panel createPrimary(String summaryText, Component content) {
        return new Panel(PanelType.PRIMARY, summaryText, content);
    }

    public Panel createPrimary(IconFactory icon, String summaryText, Component content) {
        return create(PanelType.PRIMARY, icon, summaryText, content);
    }

    public Panel createSecondary(String summaryText, Component content) {
        return new Panel(PanelType.SECONDARY, summaryText, content);
    }

    public Panel createSecondary(IconFactory icon, String summaryText, Component content) {
        return create(PanelType.SECONDARY, icon, summaryText, content);
    }

    public Panel createSuccess(String summaryText, Component content) {
        return new Panel(PanelType.SUCCESS, summaryText, content);
    }

    public Panel createSuccess(IconFactory icon, String summaryText, Component content) {
        return create(PanelType.SUCCESS, icon, summaryText, content);
    }

    public Panel createWarning(String summaryText, Component content) {
        return new Panel(PanelType.WARNING, summaryText, content);
    }

    public Panel createWarning(IconFactory icon, String summaryText, Component content) {
        return create(PanelType.WARNING, icon, summaryText, content);
    }

    public Panel createError(String summaryText, Component content) {
        return new Panel(PanelType.ERROR, summaryText, content);
    }

    public Panel createError(IconFactory icon, String summaryText, Component content) {
        return create(PanelType.ERROR, icon, summaryText, content);
    }

    private Panel create(PanelType type, IconFactory icon, String summaryText, Component content) {
        PanelConfig config = type.getConfig().toBuilder().icon(icon).build();
        return create(config, summaryText, content);
    }

    public Panel create(PanelConfig config, String summaryText, Component content) {
        return new Panel(config, summaryText, content);
    }
}
