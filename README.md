# panel-for-vaadin
Configurable panel for Vaadin. 

![preview.png](preview.png)

## Usages
The minimum configuration to create a panel is: 

```java
Panel panel = new Panel("Default panel", createContent());
```

This will create a expandable and closable panel. The default configuration comes from the
`PanelConfig` class. `PanelConfig` is responsible to hold the most important configurations of a panel, 
like: 

- background color
- text & border color
- size of border radius
- icon (and size of the icon)
- font size of the title
- padding
- width
- button variant of the close button
- closable
- collapsable (plus its default state)

`PanelConfig` also contains a builder, so you can build your custom configuration for your panel:

```java
PanelConfig customConfig = PanelConfig
        .builder()
        .width("80%")
        .closeable(false)
        .collapsable(false)
        .borderRadius("0")
        .primaryColor("#3c096c")
        .backgroundColor("#ebc9ff")
        .icon(VaadinIcon.BUG)
        .iconSize("var(--lumo-icon-size-l)")
        .variant(ButtonVariant.LUMO_CONTRAST)
        .titleFontSize("var(--lumo-font-size-xxxl)")
        .build();
Panel panel = new Panel(customConfig, "Custom panel", createContent()));
```

For the most regular usages, `Panel` contains 5 predefined `PanelType`s:

- Primary
- Secondary
- Success
- Warning
- Error

There are multiple ways to create one of the pre-configured panels:

- By passing the `PanelType` for the `Panel`
- Using the `PanelFactory`

Passing the `PanelType` is simple, you just need to pass it as a first parameter in the constructor

```java
Panel panel = new Panel(PanelType.SUCCESS, "Success panel", createContent());
```

But also a `PanelFactory` is available if you want to use (or inject) a factory and create panels 
with it. The `PanelFactory` brings some factory methods to produce these default panels: 

```java
Panel panel = panelFactory.createPrimary("Primary panel", createContent());
```

If you want to keep the configuration of a panel type, but you want to change only the icon, you 
can also use the `PanelFactory` for this: 

```java
Panel panel = panelFactory.createPrimary("Primary panel", createContent());
```

If you want to stick to a base configuration, but you want to change simple things, like the size of the text in the 
title or it's collapsable, you can use this panel type configuration as base. `PanelConfig`'s builder also comes with
a `toBuilder()`, so you easily can configure only the required stuff: 

```java
PanelConfig customConfig = PanelType.PRIMARY.getConfig()
        .toBuilder()
        .closeable(false)
        .collapsable(false)
        .icon(VaadinIcon.HANDS_UP)
        .build();
Panel panel = new Panel(customConfig, "Custom primary panel", createContent());
```

