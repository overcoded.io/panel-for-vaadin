package io.overcoded.vaadin.panel;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route("/")
public class DemoView extends VerticalLayout {

    public DemoView() {
        setWidthFull();
        setAlignItems(Alignment.CENTER);

        H1 title = new H1("Panels for Vaadin");
        title.getStyle().set("font-size", "var(--lumo-font-size-l)").set("margin", "0");


        add(title);
        add(new Panel("Default panel", createContent()));


        PanelFactory panelFactory = new PanelFactory();
        add(panelFactory.createPrimary("Primary panel", createContent()));
        add(panelFactory.createSecondary("Secondary panel", createContent()));
        add(panelFactory.createSuccess("Success panel", createContent()));
        add(panelFactory.createWarning("Warning panel", createContent()));
        add(panelFactory.createError("Error panel", createContent()));


        PanelConfig customConfig = PanelConfig
                .builder()
                .width("80%")
                .closeable(false)
                .collapsable(false)
                .borderRadius("0")
                .primaryColor("#3c096c")
                .backgroundColor("#ebc9ff")
                .icon(VaadinIcon.BUG)
                .iconSize("var(--lumo-icon-size-l)")
                .variant(ButtonVariant.LUMO_CONTRAST)
                .titleFontSize("var(--lumo-font-size-xxxl)")
                .build();
        add(new Panel(customConfig, "Custom panel", createContent()));
    }


    private Html createContent() {
        return new Html("<p>A simple primary alert with an <a href=\"https://vaadin.com/\" target=\"_blank\">example link</a>. Give it a click if you like.</p>");
    }
}
