package io.overcoded.vaadin.panel;

import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.IconFactory;
import com.vaadin.flow.component.icon.VaadinIcon;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder(toBuilder = true)
public class PanelConfig {
    private final String backgroundColor;

    @Builder.Default
    private final ButtonVariant variant = ButtonVariant.LUMO_CONTRAST;
    @Builder.Default
    private final String primaryColor = "var(--lumo-contrast)";
    @Builder.Default
    private final String iconSize = "var(--lumo-icon-size-m)";
    @Builder.Default
    private final IconFactory icon = VaadinIcon.INFO_CIRCLE_O;
    @Builder.Default
    private final String iconRightMargin = "var(--lumo-space-s)";
    @Builder.Default
    private final String borderRadius = "var(--lumo-border-radius-m)";
    @Builder.Default
    private final String padding = "var(--lumo-space-s)";
    @Builder.Default
    private final boolean closeable = true;
    @Builder.Default
    private final boolean collapsable = true;
    @Builder.Default
    private final boolean opened = true;
    @Builder.Default
    private final String width = "100%";
    @Builder.Default
    private final String titleFontSize = "var(--lumo-font-size-l)";
    @Builder.Default
    private final String titleFontWeight = "500";
}
